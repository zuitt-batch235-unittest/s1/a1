const newUser = {
  email: "juan.delacruz@gmail.com",
  password: "thequickbrownfoxjumpoverthelazydog",
};

//Activity:
const user = {
  firstName: "John",
  lastName: "Dela Cruz",
  age: 18,
  contactNumber: "09123456789",
  batchNumber: 235,
  email: "john.delacruz@gmail.com",
  password: "sixtencharacters",
};

module.exports = { newUser: newUser, user: user };
